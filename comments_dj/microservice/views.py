# Create your views here.
import json

import requests
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ViewSet

from .models import Comment
from .serializers import CommentSerializer


class CommentsViewSet(ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    model = Comment

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.event_handlers = {
            'CommentModerated': self._handle_comment_moderated,
        }

    @action(detail=False, methods=['POST'], name='add comment')
    def add_comment(self, request, post_id):
        data = request.data
        data['post'] = post_id
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        data = {
            'type': 'CommentCreated',
            'data': serializer.data
        }
        requests.post('http://event-bus-srv:4005/events/',
                      data=json.dumps(data), headers={'Content-type': 'application/json'})
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False, methods=['POST'], name='receive-event')
    def receive_event(self, request):
        print(f'Event received: {request.data.get("type")}')
        event, data = self._strip_event(request)
        try:
            res = self.event_handlers[event](data)
        except Exception as e:
            print(f'Error: {e}')

        return Response('')

    def _handle_comment_moderated(self, data):

        obj = self.queryset.get(pk=data['id'])
        obj.status = data['status']
        obj.save()
        serializer = self.serializer_class(obj)
        requests.post('http://event-bus-srv:4005/events/',
                      json.dumps({
                          'type': 'CommentUpdated',
                          'data': serializer.data
                      }),
                      headers={'Content-type': 'application/json'}
                      )

    def _strip_event(self, request):
        return request.data.get('type', 'Unknown'), request.data.get('data', {})

    @action(detail=False, methods=['GET'], name='list comments')
    def get_comments(self, request, post_id):
        result = self.queryset.filter(post=post_id)
        return Response(self.serializer_class(result, many=True).data)


# class EventViewSet(ViewSet):
#
#     @action(detail=False, methods=['POST'], name='receive-event')
#     def receive_event(self, request):
#
#         return Response(request.data)

