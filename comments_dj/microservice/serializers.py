from rest_framework.serializers import ModelSerializer

from .models import Comment


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'

    # def to_representation(self, instance):
    #     vals = super().to_representation(instance)
    #     vals.pop('post')
    #     return vals





