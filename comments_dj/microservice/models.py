import uuid

from django.db import models


# Create your models here.


class Comment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    post = models.UUIDField(null=True)
    content = models.TextField()
    status = models.CharField(max_length=16, choices=(
        ('approved', 'approved'), ('pending', 'pending'), ('rejected', 'rejected'),
    ), default='pending')
    created = models.DateTimeField(auto_now_add=True, editable=True)

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __str__(self):
        return self.content


