"""comments_dj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from microservice.views import CommentsViewSet
# path('validate-voucher-key/<str:voucher_key>', VoucherView.as_view({"get": "validate_voucher_key"})),
urlpatterns = [
    path('admin/', admin.site.urls),
    path('posts/<str:post_id>/comments/', CommentsViewSet.as_view({
        'post': 'add_comment',
        'get': 'get_comments',
    })),
    path('events/', CommentsViewSet.as_view({'post': 'receive_event'}), )
]
