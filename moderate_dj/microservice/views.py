# Create your views here.
import json

import requests
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ViewSet


class EventViewSet(ViewSet):

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.event_handlers = {
            'CommentCreated': self._handle_comment_created,
        }

    @action(detail=False, methods=['POST'], name='receive-event')
    def receive_event(self, request):
        print(f'Event received: {request.data.get("type")}')
        event, data = self._strip_event(request)
        try:
            res = self.event_handlers[event](data)
            print('Handled "{event}" event')
        except Exception as e:
            print(f'iError: {e}')

        return Response('')

    def _handle_comment_created(self, data):
        bad_words =['orange']
        test = data.get('content', '').lower().split(' ')
        if len(set(bad_words) & set(test)) > 0:
            data['status'] = 'rejected'
        else:
            data['status'] = 'approved'
        print('Sending event')
        requests.post('http://event-bus-srv:4005/events/',
                      json.dumps({
                          'type': 'CommentModerated',
                          'data': data
                      }),
                      headers={'Content-type': 'application/json'}
                      )

    def _strip_event(self, request):
        return request.data.get('type', 'Unknown'), request.data.get('data', {})
