import uuid

from django.db import models


class Comment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    post = models.UUIDField(null=True)
    content = models.TextField()
    status = models.CharField(max_length=16, choices=(
        ('approved', 'approved'), ('pending', 'pending'), ('rejected', 'rejected'),
    ), default='pending')

    class Meta:
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __str__(self):
        return self.content
