import json

import requests
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ViewSet

from .models import Post
from .serializers import PostSerializer


class PostsViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def create(self, request, *args, **kwargs):
        res = super().create(request, *args, **kwargs)
        data = {
            'type': 'PostCreated',
            'data': res.data
        }
        requests.post('http://event-bus-srv:4005/events/',
                      data=json.dumps(data), headers={'Content-type': 'application/json'})
        print("sweet!")
        return res


    @action(detail=False, methods=['POST'], name='receive-event')
    def receive_event(self, request):
        # serializer = self.serializer_class(data=request.data)
        # res = serializer.is_valid(raise_exception=False)
        print(f'Event received: {request.data.get("type")}')

        return Response({'status': 'OK'})


class EventViewSet(ViewSet):

    @action(detail=False, methods=['POST'], name='receive-event')
    def receive_event(self, request):

        return Response(request.data)


class Focus():
    def add(self, focus):
        pass

