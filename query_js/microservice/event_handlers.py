from .models import Post, Comment


def handle_post_created(data):
    obj, created = Post.objects.get_or_create(**data)
    return obj


def handle_comment_created(data):
    post = data.pop('post')
    data['post_id'] = post  # refer to the id, not the Post instance
    pk = data.pop('id')
    obj, created = Comment.objects.update_or_create(id=pk, defaults=data)
    return obj


def _strip_event(request):
    return request.data.get('type', 'Unknown'), request.data.get('data', {})


EVENT_HANDLERS = {
    'PostCreated': handle_post_created,
    'CommentCreated': handle_comment_created,
    'CommentUpdated': handle_comment_created,
}