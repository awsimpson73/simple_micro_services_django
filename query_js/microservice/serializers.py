from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Comment
from .models import Post


class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'

    def to_representation(self, instance):
        vals = super().to_representation(instance)
        vals.pop('post')
        return vals


class PostSerializer(ModelSerializer):
    comments = SerializerMethodField()


    def get_comments(self, obj):
        return CommentSerializer(obj.comments.all(), many=True).data

    class Meta:
        model = Post
        fields = ['id', 'title', 'comments']
