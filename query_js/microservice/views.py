from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .event_handlers import _strip_event, EVENT_HANDLERS
from .models import Post
from .serializers import PostSerializer


class EventViewSet(ModelViewSet):
    model = Post
    queryset = model.objects.all()
    serializer_class = PostSerializer
    event_handlers = None

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        self.event_handlers = EVENT_HANDLERS

    @action(detail=False, methods=['GET'], name='posts-list')
    def get_posts(self, request, *args, **kwargs):
        objs = self.queryset.prefetch_related('comments').all()
        serializer = PostSerializer(objs, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['POST'], name='receive-event')
    def receive_event(self, request):
        print(f'Event received: {request.data.get("type")}')
        event, data = _strip_event(request)
        try:
            res = self.event_handlers[event](data)
        except Exception as e:
            print(f'Error: {e}')
        return Response('')

