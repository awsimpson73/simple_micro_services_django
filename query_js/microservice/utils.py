import json

from .event_handlers import _strip_event, EVENT_HANDLERS
from .models import Post, Comment
import requests


def sync_from_event_bus():
    latest_post = Post.objects.latest('created').created
    latest_comment = Comment.objects.latest('created').created

    latest = latest_post if latest_post < latest_comment else latest_comment

    resp = requests.get(f"http://event-bus-srv:4005/events/{latest}", headers={
        'Content-Type': 'application/json',
        'Accept-Encoding': 'gzip, deflate, br',
    })
    if resp.status_code == 200:
        data_set = json.loads(resp.content)
        for data in data_set:
            try:
                obj = EVENT_HANDLERS[data['type']](data['data'])
            except KeyError as e:
                pass
            except Exception as e:
                pass
        return len(data_set)
    return 0


