from django.apps import AppConfig


class QueryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'microservice'

    def ready(self):
        try:
            import microservice.utils
            record_count = microservice.utils.sync_from_event_bus()
            print(f'** events synced {record_count}')
        except Exception as e:
            print(f'** Sync skipped due to no db yet {e}')
