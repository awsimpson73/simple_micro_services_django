import json

import requests
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import exceptions
from .models import Event

from .serializers import EventSerializer

DESTINATIONS = [
    'http://posts-clusterip-srv:4000/events/',
    'http://comments-clusterip-srv:4001/events/',
    'http://query-clusterip-srv:4002/events/',
    'http://moderate-clusterip-srv:4003/events/',
]


class EventViewSet(ModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    @action(detail=False, methods=['POST'], name='receive-event')
    def receive_event(self, request):
        serializer = self.serializer_class(data=request.data)
        is_valid = serializer.is_valid(raise_exception=False)
        if is_valid is True:
            serializer.save()
        for destination in DESTINATIONS:
            try:
                res = requests.post(destination,
                                    json.dumps(serializer.data), headers={'Content-type': 'application/json'}
                                    )
            except Exception as e:
                print(f'{destination} not found')

        return Response({'status': 'OK'})

    # @action(detail=False, methods=['GET'], name='sync_events')
    def sync_events(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def sync_events_from_ts(self, request, created, *args, **kwargs):
        queryset = self.queryset.filter(created__gt=created)
        return Response(self.serializer_class(queryset, many=True).data, headers={
            'Content-Type': 'application/json',

        })
