import uuid

from django.db import models


class Event(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, db_index=True)
    type = models.CharField(max_length=64, db_index=True)
    data = models.JSONField()
    created = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"
        ordering = ['created']

    def __str__(self):
        return f'{self.type} -> {self.id}'
