import React  from "react";

const CommentList = ({ comments }) => {
  const renderedComments = comments.map((comment) => {
    let content;
    content = comment.content;
    if (comment.status === 'rejected'){
        content = '<message was rejected>';
    }
    if (comment.status === 'pending') {
        content = '<message pending moderation>';
    }
    return <li key={comment.id}>{content}</li>;
  });
  return <ul>{renderedComments}</ul>;
};

export default CommentList;
